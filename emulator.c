#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>

// Emulator flags
unsigned char eDisassemble = 0;
unsigned char eTrace = 0;

typedef struct traceSymbol {
    unsigned short opcode;
    int PC;
} TraceSymbol;

TraceSymbol tracev[9999];
int tracec = 0;

// Emulation data
char* romName;
unsigned char* rom;             // rom data
int romSize;                    // size of loaded rom in bytes

typedef struct chip8State {
    char* ram;                 // whole RAM
    unsigned char V[16];       // V-registers
    unsigned short I;          // I-register
    unsigned short SP;         // Stack pointer
    unsigned short PC;         // Program counter
    unsigned char delay;
    unsigned char sound;
    unsigned char* screen;     // screen buffer
    unsigned char key[16];     // Keystates
    unsigned char romOffset;
} Chip8State;

void disassemble();
void loadROM(char* path);
void emulateCycle();
void drawScreen();
void printUsage();

Chip8State* initChip8();
void emulate();

void loadROM(char* path) {
    FILE* fp = fopen(path, "r");
	fseek(fp, 0L, SEEK_END);
	int size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	rom = malloc(size);
	fread(rom, size, 1, fp);

    romSize = size;
    printf("Loaded %d bytes\n", romSize);

	close(fp);
}

void printUsage() {
    printf("usage:\nchip8 [-d][-t] rom\n");
}

Chip8State* initChip8() {
    Chip8State* s = calloc(sizeof(Chip8State), 1);

    s->ram = calloc(4, 1024);
    s->screen = &s->ram[0xf00];
    s->SP = 0xfa0;
    s->PC = 0x200;
    s->romOffset = 0;

    return s;
}

void wait(int milliSeconds) {
    // Stroing start time
    clock_t startTime = clock();

    // looping till required time is not acheived
    while (clock() < startTime + milliSeconds*CLOCKS_PER_SEC/1000) {
        // idle
    }
}

// 0x00e0
void clr(Chip8State *state) {
    if (eDisassemble) {
        printf("clr");
        return;
    }

    clear();
}

// 0x00ee
void ret(Chip8State *state) {
    if (eDisassemble) {
        printf("ret");
        return;
    }

    state->PC = (state->ram[state->SP] << 8) | state->ram[state->SP+1];
    state->PC -= 2;
    state->SP += 2;
}

// 0x1NNN
void jmp(Chip8State *state, unsigned short destination) {
    if (eDisassemble) {
        printf("jmp #%03X", destination);
        return;
    }

    // jump and cancel PC advancement
    state->PC = destination - 2;

    if (state->romOffset) {
        state->PC += 0x200;
    }
}

// 0x2NNN
void call(Chip8State* state, unsigned short addr) {
    if (eDisassemble) {
        printf("call #%03X", addr);
        return;
    }

    state->SP -= 2;
    state->ram[state->SP  ] = ((state->PC+2) & 0xFF00) >> 8;
    state->ram[state->SP+1] = (state->PC+2) & 0x00FF;
    state->PC = addr-2;

    if (state->romOffset) {
        state->PC += 0x200;
    }
}

// 0x3XNN
void seqvi(Chip8State* state, unsigned char v, unsigned char i) {
    if (eDisassemble) {
        printf("seq #%01X, %02X", v, i);
        return;
    }

    if (state->V[v] == i) {
        state->PC+=2;
    }
}

// 0x4XNN
void snevi(Chip8State* state, unsigned char v, unsigned char i) {
    if (eDisassemble) {
        printf("sne #%01X, %02X", v, i);
        return;
    }

    if (state->V[v] != i) {
        state->PC+=2;
    }
}

// 0x5XY0
void seqvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("seq #%01X, #%01X", v1, v2);
        return;
    }

    if (state->V[v1] == state->V[v2]) {
        state->PC+=2;
    }
}

// 0x9XY0
void snevv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("sne #%01X, #%01X", v1, v2);
        return;
    }

    if (state->V[v1] != state->V[v2]) {
        state->PC+=2;
    }
}

// 0x6XNN
void movvi(Chip8State* state, unsigned char v, unsigned char i) {
    if (eDisassemble) {
        printf("ldi #%01X, %02X", v, i);
        return;
    }

    state->V[v] = i;
}

// 0x7XNN
void addvi(Chip8State* state, unsigned char v, unsigned char i) {
    if (eDisassemble) {
        printf("add #%01X, %02X", v, i);
        return;
    }

    state->V[v] += i;
}

// 0x8XY0
void movvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("mov #%01X, #%01X", v1, v2);
        return;
    }

    state->V[v1] = state->V[v2];
}

// 0x8XY1
void orvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("or #%01X, #%01X", v1, v2);
        return;
    }

    state->V[v1] = state->V[v1] | state->V[v2];
}

// 0x8XY2
void andvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("and #%01X, #%01X", v1, v2);
        return;
    }

    state->V[v1] = state->V[v1] & state->V[v2];
}

// 0x8XY3
void xorvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("xor #%01X, #%01X", v1, v2);
        return;
    }

    state->V[v1] = state->V[v1] ^ state->V[v2];
}

// 0x8XY4
void addvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("add #%01X, #%01X", v1, v2);
        return;
    }

    state->V[0xf] = (state->V[v1] + state->V[v2] > 255)? 1 : 0;
    state->V[v1] += state->V[v2];
}

// 0x8XY5
void subvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("sub #%01X, #%01X", v1, v2);
        return;
    }

    state->V[0xf] = (state->V[v1] - state->V[v2] < 0)? 0 : 1;
    state->V[v1] -= state->V[v2];
}

// 0x8XY6
void shrvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("shr #%01X, #%01X", v1, v2);
        return;
    }

    //state->V[0xf] = state->V[v1];
    state->V[0xf] = state->V[v1] & 0x00000001;
    state->V[v1] = state->V[v1] >> 1;
}

// 0x8XY7
void subbvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("sbr #%01X, #%01X", v1, v2);
        return;
    }

    state->V[0xf] = (state->V[v2] - state->V[v1] < 0)? 0 : 1;
    state->V[v1] = state->V[v2] - state->V[v1];
}

// 0x8XYE
void shlvv(Chip8State* state, unsigned char v1, unsigned char v2) {
    if (eDisassemble) {
        printf("shl #%01X, #%01X", v1, v2);
        return;
    }

    state->V[0xf] = state->V[v1] & 0b10000000;
    state->V[v1] = state->V[v1] << 1;
}

// 0xANNN
void movia(Chip8State* state, unsigned short addr) {
    if (eDisassemble) {
        printf("mov I, #%03X", addr);
        return;
    }

    state->I = addr;

    if (state->romOffset) {
        state->I += 0x200;
    }
}

// 0xBNNN
void ijp(Chip8State* state, unsigned short addr) {
    if (eDisassemble) {
        printf("ijp #%03X", addr);
        return;
    }

    state->PC = state->V[0x0] + addr;
}

// 0xCNNN
void randvi(Chip8State* state, unsigned char v, unsigned char i) {
    if (eDisassemble) {
        printf("rnd #%01X, %02X", v, i);
        return;
    }
    state->V[v] = ((unsigned char)rand()) & i;
}

// 0xDXYN
void spritevvi(Chip8State* state, unsigned char v1, unsigned char v2, unsigned char lines) {
    if (eDisassemble) {
        printf("spr #%01X, #%01X, #%01X", v1, v2, lines);
        return;
    }

    //short col, row;
    short x = state->V[v1];
    short y = state->V[v2];
    int i,j;

    state->V[0xf] = 0;
    for (i=0; i<lines; i++) {
        unsigned char *sprite = &state->ram[state->I+i];
        unsigned char spritebit=7;

        for (j=x; j<(x+8) && j<64; j++) {
            short jover8 = j / 8;     //picks the byte in the row
            short jmod8 = j % 8;      //picks the bit in the byte
            unsigned char srcbit = (*sprite >> spritebit) & 0x1;

            if (srcbit) {
                unsigned char *destbyte_p = &state->screen[ (i+y) * (64/8) + jover8];
                unsigned char destbyte = *destbyte_p;
                unsigned char destmask = (0x80 >> jmod8);
                unsigned char destbit = destbyte & destmask;

                srcbit = srcbit << (7-jmod8);

                state->V[0xf] = (srcbit & destbit)? 1:0;

                destbit ^= srcbit;
                destbyte = (destbyte & ~destmask) | destbit;
                *destbyte_p = destbyte;
            }
            spritebit--;
        }
    }
}

// 0xEX9E
void jumpkeya(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("jke #%01X", v);
        return;
    }

    if (state->key[v]) {
        state->PC+=2;
    }
}

// 0xEXA1
void jumpnkeya(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("jnk #%01X", v);
        return;
    }

    if (!state->key[v]) {
        state->PC+=2;
    }
}

// 0xFX07
void loadtimer(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("ltm #%01X", v);
        return;
    }

    state->V[v] = state->delay;
}

// 0xFX0A
void waitkey(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("war #%01X", v);
        return;
    }
}

// 0xFX15
void timer(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("tmr #%01X", v);
        return;
    }
}

// 0xFX18
void sound(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("snd #%01X", v);
        return;
    }
}

// 0xFX1E
void addiv(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("add I, #%01X", v);
        return;
    }

    state->V[0xf] = (state->I += state->V[v] > 0xfff)? 1 : 0;
    state->I += state->V[v];
}

// 0xFX29
void spriteiv(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("lsp I, #%01X", v);
        return;
    }
}

// 0xFX33
void movbcd(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("bcd #%01X", v);
        return;
    }

    unsigned char value = state->V[v];

    int ones = value % 10;
    value /= 10;
    int tens = value % 10;
    value /= 10;
    int hundreds = value;

    state->ram[state->I  ] = hundreds;
    state->ram[state->I+1] = tens;
    state->ram[state->I+2] = ones;
}

// 0xFX55
void movm(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("str #%01X", v);
        return;
    }

    int i;
    for(i = 0; i <= v; i++) {
        state->ram[state->I+i] = state->V[i];
    }
}

// 0xFX65
void fill(Chip8State* state, unsigned char v) {
    if (eDisassemble) {
        printf("get #%01X", v);
        return;
    }

    int i;
    for(i = 0; i <= v; i++) {
        state->V[i] = state->ram[state->I+i];
    }
}

void emulateCycle(Chip8State *state) {
    unsigned short opcode;
    unsigned short instruction, data;

    opcode = state->ram[state->PC] << 8 | state->ram[state->PC+1];
    instruction = opcode >> 12;
    data = opcode & 0x0fff;

    if (eTrace) {
        tracev[tracec  ].opcode = opcode;
        tracev[tracec++].PC     = state->PC;
    }

    if (eDisassemble) {
        printf("#%01x\t", instruction);
        printf("#%03x\t", data);
    }

    switch (instruction) {
        case 0x0:
            switch (data) {
                case 0x0e0: clr(state); break;
                case 0x0ee: ret(state); break;
            }
            break;
        case 0x1: jmp(state, data); break;
        case 0x2: call(state, data); break;
        case 0x3: seqvi(state, data >> 8, data & 0x00ff); break;
        case 0x4: snevi(state, data >> 8, data & 0x00ff); break;
        case 0x5: seqvv(state, data >> 8, (data >> 4) & 0x000f); break;
        case 0x6: movvi(state, data >> 8, data & 0x00ff); break;
        case 0x7: addvi(state, data >> 8, data & 0x00ff); break;
        case 0x8:
            switch (data & 0x000f) {
                case 0x0: movvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0x1: orvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0x2: andvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0x3: xorvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0x4: addvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0x5: subvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0x6: shrvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0xe: subbvv(state, data >> 8, (data >> 4) & 0x000f); break;
                case 0x7: shlvv(state, data >> 8, (data >> 4) & 0x000f); break;
            }
            break;
        case 0x9: snevv(state, data >> 8, (data >> 4) & 0x000f); break;
        case 0xa: movia(state, data); break;
        case 0xb: ijp(state, data); break;
        case 0xc: randvi(state, data >> 8, data & 0x00ff); break;
        case 0xd: spritevvi(state , data >> 8, (data >> 4) & 0x000f, data & 0x00f); break;
        case 0xe:
            switch (data & 0x00ff) {
                case 0x9e: jumpkeya(state, data >> 8); break;
                case 0xa1: jumpnkeya(state, data >> 8); break;
            }
            break;
        case 0xf:
            switch (data & 0x00ff) {
                case 0x07: loadtimer(state, data >> 8); break;
                case 0x0a: waitkey(state, data >> 8); break;
                case 0x15: timer(state, data >> 8); break;
                case 0x18: sound(state, data >> 8); break;
                case 0x1e: addiv(state, data >> 8); break;
                case 0x29: spriteiv(state, data >> 8); break;
                case 0x33: movbcd(state, data >> 8); break;
                case 0x55: movm(state, data >> 8); break;
                case 0x65: fill(state, data >> 8); break;
            }
            break;
    }

    state->PC += 2;

}

void render(Chip8State* state) {
    short x,y;
    for(y = 0; y < 32; y++) {
        for (x = 0; x < 8; x++) {
            mvprintw(y+1, x*8, "........");
            int memoryAddress = y*8+x;

            int col = 0;
            char sprite = state->screen[memoryAddress];

            for(col = 0; col < 8; col++) {
                char bit = sprite & (1 << (7-col));

                if (bit) {
                    mvprintw(y+1, x*8+col, "#");
                }
            }
        }
    }
}

void emulate() {
    initscr();
    raw();
    noecho();
    curs_set(0);
    start_color();
    cbreak();
    timeout(1);

    Chip8State* state = initChip8();

    memcpy(&state->ram[0x200], rom, romSize);

    clear();
    char input;

    while(state->PC <= 0x200+romSize) {
        mvprintw(0,0,"running\taddr: #%04X / %d", state->PC, state->PC);
        emulateCycle(state);
        render(state);
        refresh();
        wait(16);
        input = getch();

        if (input == 'q') {
            break;
        }
    }

    endwin();

    printf("last addr: #%003X\n", state->PC);

    if (eTrace) {
        int i = 0;
        for(i = 0; i < tracec; i++) {
            printf(" - #%04X\t@%04X\n", tracev[i].opcode, tracev[i].PC);
        }
    }
}

void disassemble() {
    Chip8State* state = initChip8();
    memcpy(&state->ram[0x200], rom, romSize);

    int pc;
    for(pc = 0; pc < romSize; pc+=2) {
        printf("#%04x\t", pc+0x200);
        emulateCycle(state);
        printf("\n");
    }
}

int main(int argc, char** argv) {
    if (argc == 1) {
        printUsage();
        return 0;
    }

    int argument;
    for(argument = 1; argument < argc; argument++) {
        if(strcmp(argv[argument], "-d") == 0) {
            printf("Disassembly mode\n");
            eDisassemble = 1;
        }

        else if(strcmp(argv[argument], "-t") == 0) {
            eTrace = 1;
        }

        else {
            printf("rom: %s\n", argv[argument]);
            loadROM(argv[argument]);
            break;
        }
    }

    if (eDisassemble) {
        disassemble();
        return 0;
    }

    emulate();

    return 0;
}
